package jp.alhinc.tominaga_ashiri.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		//コマンドライン引数から変数folderPathを取得する。
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		String folderPath = args[0];

		//支店コード、支店名を保持するためのマップ
		LinkedHashMap<String, String> branchNames = new LinkedHashMap<String, String>();

		//支店毎の結果を保持するためのマップ
		LinkedHashMap<String, Long> branchSales = new LinkedHashMap<String, Long>();

		//商品コード、商品名を保持するためのマップ
		LinkedHashMap<String, String> commodityNames = new LinkedHashMap<String, String>();

		//商品コード、商品の売上額を保持するためのマップ
		LinkedHashMap<String, Long> commoditySales = new LinkedHashMap<String, Long>();

		/*
		 *branch.lstの読み込み、リストへの追加と情報保持
		 *branchNamesへ支店コード、支店名を追加
		 *branchSalesへ支店コードと、合計数値を計算するための初期値(0)を追加する。
		 */
		if (!inputFromLstFile(folderPath, "branch.lst", branchNames, branchSales, "支店定義ファイル", "^[0-9]{3}$")) {
			return;
		}

		/*
		 *commodity.lstの読み込み、リストへの追加と情報保持
		 *commodityNamesへ商品コード、商品名を追加
		 *commoditySalesへ商品コードと、合計数値を計算するための初期値(0)を追加する。
		 */
		if (!inputFromLstFile(folderPath, "commodity.lst", commodityNames, commoditySales, "商品定義ファイル","^[a-zA-Z0-9]{8}$")) {
			return;
		}

		/*
		 *ファイル名が数字8桁＋“.rcd”のもののみを選択してrcdFilesに格納。
		 *またファイル名はソートしておく。
		 *fileListに保持しているファイル情報を数字8桁＋rcdのファイルが連番になっているかを確認する
		 */
		File allFilesPath;
		ArrayList<File> rcdFiles = new ArrayList<File>();
		try {
			allFilesPath = new File(folderPath);
			for (File filePath : allFilesPath.listFiles()) {
				if (filePath.getName().matches("[0-9]{8}\\.rcd$")) {
					rcdFiles.add(filePath);
				}
			}
			Collections.sort(rcdFiles);

			for (int i = 0; i < rcdFiles.size() - 1; i++) {
				String firstFileName = rcdFiles.get(i).getName();
				String secondFileName = rcdFiles.get(i + 1).getName();
				int firstFilesNum = Integer.parseInt(firstFileName.substring(0, firstFileName.lastIndexOf(".")));
				int secondFilesNum = Integer.parseInt(secondFileName.substring(0, secondFileName.lastIndexOf(".")));
				if (firstFilesNum != secondFilesNum - 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
		} catch (IndexOutOfBoundsException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		/*rcdファイルに関する処理の開始
		 *リスト変数のsplitRcdFileContent→ファイルに記載の支店番号と商品名、金額を保持するための変数
		 *支店毎の結果を保持するマップ変数のbranchesSalesに各支店毎で金額を加算していく
		 *商品ごとの結果を保持するマップ変数のcommoditySalesに各商品ごとで金額を加算していく
		 */
		FileReader rcdFileReader = null;
		BufferedReader rcdBufferedReader = null;
		for (File fileName : rcdFiles) {
			try {

				ArrayList<String> splitRcdFileContent = new ArrayList<String>();

				rcdFileReader = new FileReader(fileName);
				rcdBufferedReader = new BufferedReader(rcdFileReader);
				String rcd;
				while ((rcd = rcdBufferedReader.readLine()) != null) {
					splitRcdFileContent.add(rcd);
				}
				if (splitRcdFileContent.size() != 3) {
					System.out.println(fileName.getName() + "のフォーマットが不正です");
					return;
				}
				if (!branchSales.containsKey(splitRcdFileContent.get(0))) {
					System.out.println(fileName.getName() + "の支店コードが不正です");
					return;
				}
				if (!commoditySales.containsKey(splitRcdFileContent.get(1))) {
					System.out.println(fileName.getName() + "の商品コードが不正です");
					return;
				}
				Long branchSale = (Long) branchSales.get(splitRcdFileContent.get(0))
						+ Long.parseLong(splitRcdFileContent.get(2));
				Long commoditySale = (Long) commoditySales.get(splitRcdFileContent.get(1))
						+ Long.parseLong(splitRcdFileContent.get(2));
				if (branchSale.toString().matches("^[0-9]{10,}$") || commoditySale.toString().matches("^[0-9]{10,}$")) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.replace(splitRcdFileContent.get(0), branchSale);
				commoditySales.replace(splitRcdFileContent.get(1), commoditySale);
			} catch (IOException | NumberFormatException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				try {
					if (rcdBufferedReader != null) {
						rcdBufferedReader.close();
					}
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		//支店番号、支店名、各支店の集計結果をファイルに書き出す。
		if (!outputToOutFile(folderPath, "branch.out", branchNames, branchSales)) {
			return;
		}
		//商品コード、商品名、各商品の集計結果をファイルに書きだす。
		if (!outputToOutFile(folderPath, "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 定義ファイル読み込み(入力)のメソッド
	 * @author tominaga.ashiri
	 *
	 * @param folderPath 定義ファイルが格納されているフォルダのPath
	 * @param fileName 定義ファイル名
	 * @param namesMap 名称とその名称のコードを格納するマップ
	 * @param salesMap 名称のコードとその名称の売り上げを格納するマップ
	 * @param errorMessage コンソールへのエラーメッセージ表示時に出したいファイル名
	 * @param regularExpression 対象の定義ファイルに対して確認したい正規表現
	 * @return boolean
	 */
	public static boolean inputFromLstFile(String folderPath, String fileName, Map<String, String> namesMap,
			Map<String, Long> salesMap, String errorMessage, String regularExpression) {

		File file = null;
		FileReader fr = null;
		BufferedReader br = null;
		try {
			file = new File(folderPath, fileName);
			if (!file.exists()) {
				System.out.println(errorMessage + "が存在しません");
				return false;
			}
			fr = new FileReader(file);
			br = new BufferedReader(fr);

			String bufferInfo;
			while ((bufferInfo = br.readLine()) != null) {
				String[] splitInfo = bufferInfo.split(",", -1);
				if (!splitInfo[0].matches(regularExpression) || splitInfo.length != 2) {
					System.out.println(errorMessage + "のフォーマットが不正です");
					return false;
				}
				namesMap.put(splitInfo[0], splitInfo[1]);
				salesMap.put(splitInfo[0], (long) 0);
			}
			return true;
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
	}

	/**
	 *集計ファイルへの出力用メソッド
	 * @author tominaga.ashiri
	 *
	 * @param folderPath 定義ファイルが格納されているフォルダのPath
	 * @param fileName 定義ファイル名
	 * @param namesMap 名称とその名称のコードを格納するマップ
	 * @param salesMap 名称のコードとその名称の売り上げを格納するマップ
	 * @return boolean
	 */
	public static boolean outputToOutFile(String folderPath, String fileName,
			Map<String, String> namesMap, Map<String, Long> salesMap) {
		File file = null;
		FileWriter fw = null;
		BufferedWriter bw = null;
		PrintWriter pw = null;
		try {
			file = new File(folderPath, fileName);
			fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);
			for (Map.Entry<String, String> information : namesMap.entrySet()) {
				String recordRow = information.getKey() + ","
						+ information.getValue() + "," + salesMap.get(information.getKey());
				pw.println(recordRow);
			}
			return true;

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (pw != null) {
				pw.close();
			}
		}
	}
}
